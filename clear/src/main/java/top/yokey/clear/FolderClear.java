package top.yokey.clear;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Vector;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class FolderClear implements IXposedHookLoadPackage {

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) {
        //本机文件
        String path;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
        } else {
            path = Environment.getDataDirectory().getAbsolutePath() + "/data/";
        }
        File[] files = new File(path).listFiles();
        //要删除的文件
        Vector<String> folder = new Vector<>();
        BaseFileClient.get().init("top.yokey.clear");
        try {
            File setting = new File(BaseFileClient.get().getCachePath() + "setting.txt");
            if (setting.isFile() && setting.exists()) {
                InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(setting));
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String content = line.split("\\+")[0];
                    folder.add(content);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < folder.size(); i++) {
            for (File file : files) {
                if (folder.get(i).equals(file.getName())) {
                    if (folder.get(i).equals(file.getName())) {
                        if (FileUtil.delete(file.getAbsolutePath())) {
                            XposedBridge.log("YokeyClear删除：" + file.getName() + " 成功！");
                        } else {
                            XposedBridge.log("YokeyClear删除：" + file.getName() + " 失败！");
                        }
                    }
                }
            }
        }
    }

}
