package top.yokey.clear;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class MainActivity extends AppCompatActivity {

    private File[] files;
    private Vector<String> folder;
    private FileListAdapter mainAdapter;

    private AppCompatTextView deleteTextView;
    private RecyclerView mainRecyclerView;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        BaseFileClient.get().init("top.yokey.clear");
        initView();
        initData();
        initEvent();
    }

    private void initView() {

        deleteTextView = findViewById(R.id.deleteTextView);
        mainRecyclerView = findViewById(R.id.mainRecyclerView);

    }

    private void initData() {

        String path;

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
        } else {
            path = Environment.getDataDirectory().getAbsolutePath() + "/data/";
        }

        folder = new Vector<>();
        try {
            File setting = new File(BaseFileClient.get().getCachePath() + "setting.txt");
            if (setting.isFile() && setting.exists()) {
                InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(setting));
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String content = line.split("\\+")[0];
                    folder.add(content);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<File> mainArrayList = new ArrayList<>();
        ArrayList<File> folderArrayList = new ArrayList<>();
        ArrayList<File> fileArrayList = new ArrayList<>();
        files = new File(path).listFiles();
        for (File file : files) {
            if (file.isDirectory() && !file.getName().substring(0, 1).equals(".")) {
                folderArrayList.add(file);
            }
            if (!file.isDirectory() && !file.getName().substring(0, 1).equals(".")) {
                fileArrayList.add(file);
            }
        }

        Collections.sort(folderArrayList, new FileComparator());
        Collections.sort(fileArrayList, new FileComparator());
        mainArrayList.addAll(folderArrayList);
        mainArrayList.addAll(fileArrayList);

        mainAdapter = new FileListAdapter(mainArrayList, folder);
        mainRecyclerView.setAdapter(mainAdapter);
        mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private void initEvent() {

        mainAdapter.setOnItemClickListener(new FileListAdapter.onItemClickListener() {
            @Override
            public void onClick(int position, File file, boolean isClick) {
                String name = file.getName();
                if (isClick) {
                    boolean flag = true;
                    for (int i = 0; i < folder.size(); i++) {
                        if (folder.get(i).equals(name)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        folder.add(name);
                    }
                } else {
                    for (int i = 0; i < folder.size(); i++) {
                        if (folder.get(i).equals(name)) {
                            folder.remove(i);
                            break;
                        }
                    }
                }
                saveFile();
            }
        });

        deleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < folder.size(); i++) {
                    for (File file : files) {
                        if (folder.get(i).equals(file.getName())) {
                            Log.d("YokeyClear", "删除：" + file.getName());
                            if (folder.get(i).equals(file.getName())) {
                                if (FileUtil.delete(file.getAbsolutePath())) {
                                    Log.d("YokeyClear", "删除：" + file.getName() + " 成功！");
                                } else {
                                    Log.d("YokeyClear", "删除：" + file.getName() + " 失败！");
                                }
                            }
                        }
                    }
                }
            }
        });

    }

    @SuppressWarnings("StringConcatenationInLoop")
    private void saveFile() {

        try {
            String content = "";
            for (int i = 0; i < folder.size(); i++) {
                content += folder.get(i) + "\n";
            }
            File setting = new File(BaseFileClient.get().getCachePath() + "setting.txt");
            setting.createNewFile();
            FileOutputStream fos = new FileOutputStream(setting);
            byte[] buffer = content.getBytes();
            fos.write(buffer, 0, buffer.length);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
