package top.yokey.clear;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

public class FileListAdapter extends RecyclerView.Adapter<FileListAdapter.ViewHolder> {

    private Vector<String> folder;
    private ArrayList<File> arrayList;
    private onItemClickListener onItemClickListener;

    FileListAdapter(ArrayList<File> arrayList, Vector<String> folder) {
        this.folder = folder;
        this.arrayList = arrayList;
        this.onItemClickListener = null;
    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final int positionInt = position;
        final File file = arrayList.get(positionInt);

        holder.mainCheckBox.setChecked(false);
        holder.mainTextView.setText(file.getName());
        if (file.isDirectory()) {
            holder.mainImageView.setImageResource(R.mipmap.ic_folder);
        } else {
            holder.mainImageView.setImageResource(R.mipmap.ic_file);
        }
        for (int i = 0; i < folder.size(); i++) {
            if (folder.get(i).equals(file.getName())) {
                holder.mainCheckBox.setChecked(true);
            }
        }

        holder.mainLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mainCheckBox.setChecked(!holder.mainCheckBox.isChecked());
                if (onItemClickListener != null) {
                    onItemClickListener.onClick(positionInt, file, holder.mainCheckBox.isChecked());
                }
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_list_file, group, false);
        return new ViewHolder(view);
    }

    public void setOnItemClickListener(onItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface onItemClickListener {

        void onClick(int position, File file, boolean isClick);

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayoutCompat mainLinearLayout;
        private AppCompatImageView mainImageView;
        private AppCompatTextView mainTextView;
        private AppCompatCheckBox mainCheckBox;

        private ViewHolder(View view) {
            super(view);

            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            mainTextView = view.findViewById(R.id.mainTextView);
            mainCheckBox = view.findViewById(R.id.mainCheckBox);

        }

    }

}
